#! /bin/bash

# TODO: Create a Plugin system

# Default content type to text/html
CONTENTTYPE="text/html"

# Check: Is it a Directory, a PHP script or something else?
if [[ "$FILE" == *.php ]] && [ "$ENABLE_PHP" = "true" ]; then
	. ./httpheaders.sh
	php "$FILE" >> $OUTFILE
elif [[ "$FILE" == *.md ]] && [ "$ENABLE_MARKDOWN" = "true" ]; then
	. ./httpheaders.sh
	pandoc -f gfm "$FILE" >> $OUTFILE
elif [ -f "${FILE}" ]; then
	CONTENTTYPE=$(file -b -i "$FILE")
	. ./httpheaders.sh
	cat "${FILE}" >> ${OUTFILE}
else
	. ./httpheaders.sh
	. ./filelist.sh
fi
