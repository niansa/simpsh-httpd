alias git='"true" ' &&
echo "Installing depencies..." &&
apt update -qq &&
apt install -qq -y fakeroot curl &&
echo "Building Debian package..." &&
mkdir /tmp/simpsh-httpd &&
mv ./* /tmp/simpsh-httpd/ &&
mv ./.git /tmp/simpsh-httpd/ &&
export HOME="/tmp/" &&
cd &&
chmod -R a+x ./simpsh-httpd &&
./simpsh-httpd/mkdeb.sh &&
apt -y install ./simpsh-httpd.deb &&
echo "Starting simpsh-httpd..." &&
mkdir -p /var/www/html &&
simpsh-httpd-socat -d &&
echo "Creating test document..." &&
(echo "This is a test!" > /var/www/html/testdocument.txt) &&
echo "Loading test document from server..."
testdoc="$(curl http://localhost:8888/testdocument.txt)" &&
(if [[ $testdoc == "This is a test!" ]]; then echo "Server returned expected document"; else echo "Something went wrong... Server returned: $testdoc"; exit 1; fi)