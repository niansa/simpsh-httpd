#! /bin/bash

# Variable initalisation
. ./init.sh

# Read the config file
. ./config.sh

# Read the HTTP-request
. ./readrequest.sh

# Some URL checks and modifications
. ./urlcheck.sh

# Skip following step, if the document was already created successfully
if [ "$DONE" != "true" ]; then
	# Write finished file
	. ./sendfile.sh
fi

# Print finished document
cat $OUTFILE

# Remove the temporary file
rm $OUTFILE
