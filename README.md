# simpsh-httpd


## Installation

### *Recomended*: Release installation 
1. Download a release from the [Tags](https://gitlab.com/niansa/simpsh-httpd/tags/) page
2. Follow step 4 and 5 of "Installation as package" if you selected to download and install the Prebuild Debian package. Otherwise follow step 3-6 of "Standard installation"

### Installation as package (Debian based distros only)
1. Install build dependencies: `sudo apt install fakeroot curl git`
2. Build package: `curl https://gitlab.com/niansa/simpsh-httpd/raw/master/mkdeb.sh | bash -`
3. Install package: `sudo apt install ~/simpsh-httpd.deb`
4. Edit /etc/simpsh-httpd/config.sh: `sudoedit /etc/simpsh-httpd/config.sh`
5. Start simpsh-httpd: `simpsh-httpd-socat -d`

### Standard installation
1. Install following packages: `bash socat dos2unix`
2. Download simpsh-httpd:
 * `$ cd ~`
 * `$ git clone https://github.com/niansa/simpsh-httpd.git`
 * `$ cd ./simpsh-httpd`
3. Edit ./config.sh: `nano ./config.sh`
4. Make all files executable
 * `chmod -R a+x ~/simpsh-httpd`
5. Start simpsh-httpd:
 * `cd ~/simpsh-httpd`
 * `./start-socat.sh -d`
6. Have fun with simpsh-httpd!


## Similar projects

### XMB-HTTPD-2
http://xmb.ovh/codes/xmb-httpd-2 (Webpage is down)
Another HTTPD which is written in bash.

### Iwanttobash
https://github.com/plutesci/Iwanttobash
A tool for bash scripting with options for python html etc.


## Entstehung
Irgendwann Mitte des Jahres 2016 fing ich an das HTTP-Protokoll zu 
untersuchen. Ich fand es so interessant, dass ich anfing einen 
Webserver (HTTPD) zu schreiben! Damals habe ich Bash für alles 
eingesetzt, also setzte ich es auch für den Webserver ein... Ich 
hatte damals auf einem shell Provider (bitcoinshell) ein Kommando 
entdeckt, was mir den Inhalt eines Ordners in HTML als Liste 
zurückgab. Da ich dachte, mit dem Kommando ließe sich ein 
Dateiindex wohl sehr einfach einbauen, fing ich damit an. Es wurde 
aber schwer, als ich feststellte, dass das Kommando nur dort zu 
finden war! Also schrieb ich den Dateiindexer selber. Nun kam der 
nächste schwere Punkt: kann man auf einem Port lauschen? Ich 
probierte es erst mit netcat, was sich als sehr schwierig erwies, 
schlussendlich erfüllte socat den Zweck.
Und so fing dieses kleine Projekt an...
