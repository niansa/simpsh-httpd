#! /bin/bash

# Block tries to read documents outside the document root
if [[ "$URL" == *..* ]]; then
	URL='/'
	FILE="$FILES"
fi

# Avoid a bug
if [ -d "$FILE" ]; then
	if [[ "$URL" != */ ]]; then
		STATUS='HTTP/1.0 302 Moved permanently
Location: '"$URL"'/'
		CONTENTTYPE="text/html"
		. ./httpheaders.sh
		DONE=true
	fi
fi

# Find index.html
if [ -f "${FILE}index.html" ]; then
	URL="${URL}index.html"
	FILE="${FILE}index.html"
fi

# Find index.php
if [ -f "${FILE}index.php" ]; then
	URL="${URL}index.php"
	FILE="${FILE}index.php"
fi

# Respond 404 if the file doesn't exist
if [ ! -e "$FILE" ]; then
        STATUS='HTTP/1.0 404 Not Found'
        CONTENTTYPE='text/html'
	FILE="$ERROR404"
fi

# Respond 403 if the file isn't readable
if [ ! -r "$FILE" ]; then
        STATUS='HTTP/1.0 403 Forbidden'
        CONTENTTYPE='text/html'
	FILE="$ERROR403"
fi
