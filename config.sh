#! /bin/bash

# The web server is running under this user (requires to start the socat script as root!). This option may be empty
SUUSER=""

# Here is the webroot (homefolder for the website)
FILES="/var/www/html/"

# The finished Document will saved here. ${OUTFILEID} must be included!
OUTFILE="/tmp/simpsh-html.${OUTFILEID}.html"

# This port will used for listening
PORT=8888

# Should PHP be enabled? You need to install php-cli package to use it. May be false or true
ENABLE_PHP="false"

# Should Markdown be enabled? You need to install pandoc package to use it. May be false or true
ENABLE_MARKDOWN="false"

# Error code pages, they need to be HTML documents
ERROR404="${FILES}/404.html"
ERROR403="${FILES}/403.html"
