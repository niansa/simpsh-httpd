#! /bin/bash

# Read the request
read header

# Process the request
URL="${header#GET }"
URL="${URL% HTTP/*}"
FILE="$FILES$URL"
DECODED_FILE=$(printf '%b' "${FILE//%/\\x}")
FILE="$DECODED_FILE"
