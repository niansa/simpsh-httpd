#! /bin/bash

echo '<html>
	<head>
		<title>Index of: '"$URL"'</title>
	</head>
	<body>
		<p>'"$URL"'</p>
		<h2>Directory list:</h2><br />
		<a href="../">..</a><br />' >> $OUTFILE

for i in $( ls "${FILE}" ); do
	echo '		<a href="./'"${i}"'">'"${i}"'</a><br />' >> $OUTFILE
done

echo '	</body>
</html>' >> $OUTFILE
