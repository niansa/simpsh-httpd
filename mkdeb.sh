# Change to users home
cd

# Clone the repository
git clone https://gitlab.com/niansa/simpsh-httpd.git

# Make the mkpackage script executable
chmod +x ./simpsh-httpd/mkpackage.sh

# Call package-generator script
./simpsh-httpd/mkpackage.sh ~/simpsh-httpd/ ~/simpsh-httpd-pkg/

# Create Package info
mkdir ~/simpsh-httpd-pkg/DEBIAN
cd ./simpsh-httpd/
echo 'Package: simpsh-httpd
Version: '"$(git rev-list --all --count)"'
Maintainer: nisa/niansa <nisa@bitcoinshell.mooo.com>
Priority: extra
Architecture: all
Depends: bash, socat
Recommends: php-cli, pandoc
Description: An usable bash-httpserver with easy configuration and very simple PHP' > ~/simpsh-httpd-pkg/DEBIAN/control
cd ..

# Build package
fakeroot dpkg -b ~/simpsh-httpd-pkg ~/simpsh-httpd.deb

# Clean up
rm -rf ~/simpsh-httpd-pkg
rm -rf ~/simpsh-httpd
